import unittest

class SimpleDemonstrationTest(unittest.TestCase):
    @unittest.skip("Test skipped for demonstration purposes")
    def test_skipped(self):
        self.fail("This result should not be visible")

    def test_pass(self):
        self.assertEqual(10, 7 + 3)

    def test_fail(self):
        self.assertEqual(11, 7 + 4)

if __name__ == '__main__':
    import xmlrunner
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
